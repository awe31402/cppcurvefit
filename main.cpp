#include <iostream>
#include <ceres/ceres.h>
#include <Eigen/Core>
#include <random>
#include <fstream>
#include  "gnuplot-iostream.h"

using namespace std;
using namespace Eigen;

#define RANDOM_MEAN (0.0)
#define RANDOM_VARIANCE (2.0)
#define A (-1.0)
#define B (2.0)
#define C (3.0)

class Data {
public:
    double x;
    double y;
    double real;
    double estimate;
    friend ostream& operator<< (ostream& out, const Data& curr );
};

ostream& operator<< (ostream& out, const Data& curr )
{
    out << curr.x << " " << curr.y << " " << curr.real
        << " " << curr.estimate << endl;
    return out;
}

typedef vector<Data> DataSet;

struct CURVE_FITTING_COST
{
    CURVE_FITTING_COST(double x, double y):
        _x(x), _y(y){}

    template <typename T>
    bool operator() ( const T* const abc,
     T* residual) const
    {
        residual[0] = T(_y) - 
            ceres::exp(abc[0] * _x * _x + abc[1] *_x + abc[2]);
        return true;
    }

    const double _x, _y;
};

int main(int argc, char** argv)
{
    double a = A;
    double b = B;
    double c = C;

    int nData = 400;
    DataSet data(nData);
   
    double abc[3] = {0.0, 0.0, 0.0};

    default_random_engine generator;
    normal_distribution<double> distribution(RANDOM_MEAN, RANDOM_VARIANCE);

    VectorXd x_data(nData);
    VectorXd y_data(nData);

    for (int i =0; i < nData; i++) {
        double dNoise = distribution(generator);
        double x = i / 100.0;
        double real = exp(a * x * x + b * x + c );
        double y = real + dNoise;
        
        x_data(i) = x;
        y_data(i) = y;
        data[i].x = x;
        data[i].y = y;
        data[i].real = real;
    }

    ceres::Problem problem;
    
    for (int i =0 ;i < nData; i++) {
        problem.AddResidualBlock(
            /* 1 is dimension for residual
               3 is dimension for parameters*/
            new ceres::AutoDiffCostFunction<CURVE_FITTING_COST, 1, 3>(
                new CURVE_FITTING_COST(x_data(i), y_data(i))
            ),
            NULL,
            abc
        );
    }

    ceres::Solver::Options options;
    options.linear_solver_type = ceres::DENSE_QR;
    options.minimizer_progress_to_stdout = true;

    ceres::Solver::Summary summary; 
    ceres::Solve(options, &problem, &summary);

    cout << summary.FullReport() << endl;
 
    cout << "real a = " << a
         << " b = " << b
         << " c = " << c << endl;

    cout << "estimated a = " << abc[0]
         << " b = " << abc[1]
         << " c = " << abc[2] << endl;

    cout << "Saving Estimate result" << endl;

    ofstream fout;
    fout.open("data.txt", ios::out);
    
    for (int i =0; i < nData; i++) {
        double x = data[i].x;
        double y = data[i].y;
        double dReal = data[i].real;
        double dEstimate = exp(abc[0] * x * x + abc[1] * x + abc[2]);
        fout << x << "\t" << y_data(i) << "\t"
             << dReal << "\t" << dEstimate << endl;;
        data[i].estimate = dEstimate;
    }

    fout.close();

    Gnuplot gp;
    gp << "set grid" << endl;
    gp << "show grid" << endl;
    gp << "plot " << gp.file1d(data)
     << "  using 1:2 with points lt rgb 'blue' title 'data with noise', "
     << gp.file1d(data) << "  using 1:3 with linespoints linewidth 2 lt rgb 'black' title 'real data', "
     << gp.file1d(data) << "  using 1:4 with linespoints linewidth 2 lt rgb 'green' title 'estimate data'\n";
    return 0;
}
